# Instructions

## Folder structure

```
STGAN/
    - <dataset>/
        - data/
            - data.npy
            - ...
        - checkpoint/
        - result/
```